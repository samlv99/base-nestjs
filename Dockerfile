### STAGE 1: Install dependencies and build
FROM node:16.15.0-alpine AS build
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile --silent
COPY . .
RUN yarn build && yarn install --production --silent

### STAGE 2: Production
FROM node:16.15.0-alpine
LABEL author='Lai Van Sam <samlv3112@gmail.com>'
WORKDIR /usr/src/app
COPY --from=build /usr/src/app/dist /usr/src/app/dist
COPY --from=build /usr/src/app/node_modules /usr/src/app/node_modules
COPY --from=build /usr/src/app/package.json /usr/src/app/package.json
COPY --from=build /usr/src/app/.env /usr/src/app/.env

EXPOSE 3000
CMD ["yarn", "start:prod"]