import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { mailInfo } from 'common/mail-info';
import { MailService } from 'mail/mail.service';
import * as moment from 'moment';

@Injectable()
export class ScheduleService {
  constructor(private mailService: MailService) {}
  @Cron(CronExpression.EVERY_30_SECONDS, { timeZone: 'UTC' })
  async sendMail() {
    console.log('start send mail');
    const transportName = 'info';
    await this.mailService.sendMail(
      {
        ...mailInfo().EVENT_START,
        context: {
          startDate: moment().format('YYYY-MM-DD HH:mm'),
          fullNameUser: 'Lai Van Sam',
          nameEvent: 'Start Event',
          place: 'Ha Noi',
        },
        to: 'samcrseven3112@gmail.com',
      },
      transportName
    );
  }
}
