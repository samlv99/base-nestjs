import { Module } from '@nestjs/common';
import { ScheduleModule as CronjobModule } from '@nestjs/schedule';
import { MailModule } from 'mail/mail.module';
import { ScheduleService } from './schedule.service';

@Module({
  imports: [CronjobModule.forRoot(), MailModule],
  providers: [ScheduleService],
})
export class ScheduleModule {}
