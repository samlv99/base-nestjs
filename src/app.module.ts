import { Logger, Module, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD, APP_PIPE } from '@nestjs/core';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CmsAuthModule } from 'app/http/cms/auth/auth.module';
import { AuthModule } from 'app/http/app/auth/auth.module';
import { UserModule } from 'app/http/cms/user/user.module';
import { createConnection } from 'typeorm';
import { MailModule } from './app/http/app/mail-schedule/mail-schedule.module';
import { ScheduleModule } from 'schedule/schedule.module';
import config from './config';

const logger = new Logger('AppModule');

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [config],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => configService.get('database'),
      dataSourceFactory: async (options) => {
        try {
          const connection = await createConnection(options);
          logger.log('Mysql connect successfully!');
          return connection;
        } catch (error) {
          logger.error(error);
        }
      },
    }),
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 60,
    }),
    CmsAuthModule,
    AuthModule,
    UserModule,
    ScheduleModule,
    MailModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
  ],
})
export class AppModule {}
