interface Context {
  nameEvent?: string;
  fullNameUser?: string;
  place?: string;
  startDate?: string;
  otpCode?: number;
}

export interface MailI {
  to: string;
  subject: string;
  template?: string;
  html?: string;
  context?: Context;
}
