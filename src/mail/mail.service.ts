import { MailerService } from '@nest-modules/mailer';
import { Injectable, Logger } from '@nestjs/common';
import { MailI } from 'mail';

const logger = new Logger('MailService');

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}
  async sendMail(mailInfo: MailI, transporterName?: string) {
    try {
      await this.mailerService.sendMail({
        ...mailInfo,
        transporterName: transporterName || 'noReply',
        from: transporterName == 'info' ? process.env?.INFO_MAIL_USER : process.env?.MAIL_USER,
      });
      logger.debug('sendMail success');
    } catch (e) {
      logger.error(e);
    }
  }
}
