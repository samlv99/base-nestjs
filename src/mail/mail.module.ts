import { HandlebarsAdapter, MailerModule } from '@nest-modules/mailer';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MailService } from './mail.service';

@Module({
  imports: [
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => {
        const mailer = config.get('mailer');
        const transport = {
          host: mailer.host,
          secure: mailer.secure,
        };
        return {
          transports: {
            noReply: {
              ...transport,
              auth: {
                user: mailer.noReply.user,
                pass: mailer.noReply.password,
              },
            },
            info: {
              ...transport,
              auth: {
                user: mailer.info.user,
                pass: mailer.info.password,
              },
            },
          },
          template: {
            dir: process.cwd() + '/src/templates',
            adapter: new HandlebarsAdapter(),
            options: {
              strict: false,
            },
          },
        };
      },
    }),
  ],
  exports: [MailService],
  providers: [MailService],
})
export class MailModule {}
