const config = () => ({
  appName: process.env.APP_NAME,
  environment: process.env.NODE_ENV,
  port: process.env.APP_PORT,
  database: {
    type: process.env.DB_TYPE,
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    synchronize: true,
    maxConnections: parseInt(process.env.DB_MAX_CONNECTIONS, 10) || 100,
    supportBigNumbers: false,
    logging: false,
    charset: 'utf8mb4',
    migrationsTableName: 'migration',
    entities: ['dist/app/entities/**/*.js'],
    timezone: 'Z',
  },
  auth: {
    saltRounds: Number(process.env.SALT_ROUNDS),
    accessTokenExpire: Number(process.env.ACCESS_TOKEN_EXPIRE),
    refreshTokenExpire: Number(process.env.REFRESH_TOKEN_EXPIRE),
    accessTokenSecret: process.env.ACCESS_TOKEN_SECRET,
    refreshTokenSecret: process.env.REFRESH_TOKEN_SECRET,
    verificationCodeExpire: Number(process.env.VERIFICATION_CODE_EXPIRE),
  },
  mailer: {
    host: process.env.EMAIL_HOST || '',
    secure: process.env.MAIL_SECURE == 'true' || false,
    ttl: process.env.OTP_TTL || 120,
    noReply: {
      user: process.env.MAIL_USER || '',
      password: process.env.MAIL_PASSWORD || '',
    },
    info: {
      user: process.env?.INFO_MAIL_USER || '',
      password: process.env?.INFO_MAIL_PASSWORD || '',
    },
  },
});

export default config;
