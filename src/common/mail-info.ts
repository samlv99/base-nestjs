export const mailInfo = (fullNameUser?: string) => ({
  SEND_OTP: {
    subject: '【Tiny Market】Bạn chưa hoàn thành đăng ký của mình',
    template: './send-otp',
  },
  EVENT_START: {
    subject: `【Tiny Market】Thông báo tổ chức sự kiện`,
    template: './event-start',
  },
  JOIN_EVENT: {
    subject: `【Tiny Market】Thông báo tham gia sự kiện`,
    template: './joins-event',
  },
  CHANGE_MAIL: {
    subject: '【Tiny Market】Địa chỉ Email của bạn chưa được thay đổi',
    template: './change-mail',
  },
  FORGOT_PASSWORD: {
    subject: '【Tiny Market】Cấp lại mật khẩu chưa được hoàn thành',
    template: './forgot-pw',
  },
});
