export enum ErrorCode {
  EmailIsAlreadyExist = 1,
  UserNotExits = 2,
  PasswordIsIncorector = 3,
  RefreshTokenNotExits = 4,
  UserBlocked = 5,
  InvalidSignature = 6,
  NewPasswordIsNotSameOldPassword = 7,
  TokenExpired = 8,
  AccessTokenInvalid = 9,
  AccessTokenNotExist = 10,
  MobileIsAlreadyExist = 11,
  MobileNotExist = 12,
  MaximumRetry = 13,
  VerificationCodeInvalid = 14,
}

export enum ErrorMessage {
  PasswordIsTooShort = 'password is too short',
}

export enum CommonStatus {
  InActive = 0,
  Active = 1,
}

export enum AuthType {
  Normal = 0,
  Facebook = 1,
}

export enum VerificationCodeType {
  Register = 1,
  Login = 2,
  ForgotPassword = 3,
}

export enum Environment {
  Development = 'development',
  Staging = 'staging',
  Production = 'production',
}
