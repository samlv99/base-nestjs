import { HttpException, HttpStatus } from '@nestjs/common';
import { ErrorCode } from 'app/enums';
import { random } from 'lodash';

export function AppError(errorCode: ErrorCode, status?: HttpStatus) {
  throw new HttpException({ errorCode }, status ? status : HttpStatus.BAD_REQUEST);
}

export function randomOTP(length: number) {
  const numbers = [];
  for (let i = 0; i < length; i++) {
    numbers.push(random(0, 9));
  }

  return numbers.join('');
}

export function handleInputPagination(params) {
  params.pageIndex = Number(params.pageIndex) || 1;
  params.take = Number(params.take) || 10;
  params.skip = (params.pageIndex - 1) * params.take;

  return params;
}

export function handleOutputPagination(data: any, totalItems: number, params, metadata = {}) {
  return {
    data,
    totalItems,
    paging: true,
    pageIndex: params.pageIndex,
    totalPages: Math.ceil(totalItems / params.take),
    hasMore: data ? (data.length < params.take ? false : true) : false,
    metadata,
  };
}
