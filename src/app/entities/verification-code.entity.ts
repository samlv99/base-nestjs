import { CommonStatus, VerificationCodeType } from 'app/enums';
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity('verification_code')
export default class VerificationCode {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'mobile', type: 'varchar', length: 20 })
  mobile: string;

  @Column({ name: 'code', type: 'varchar', length: 20 })
  code: string;

  @Column({ name: 'email', type: 'varchar', length: 255 })
  email: string;

  @Column({ name: 'retry', type: 'tinyint', default: 0, comment: 'Count retry code' })
  retry: number;

  @Column({
    name: 'status',
    type: 'tinyint',
    default: VerificationCodeType.Register,
    comment: '1: Active, 0: inactive',
  })
  status: number;

  @Column({
    name: 'type',
    type: 'tinyint',
    default: CommonStatus.Active,
    comment: '1: Register, 2: Login, 3: Forgot Password',
  })
  type: number;

  @Column({ name: 'expire_date', type: 'datetime' })
  expireDate: Date | string;

  @Column({ name: 'vertifycation_date', type: 'datetime' })
  vertifycationDate: Date | string;

  @CreateDateColumn({ name: 'created_date', type: 'datetime', nullable: true })
  createdAt: Date | string;

  @UpdateDateColumn({ name: 'update_date', type: 'datetime', nullable: true })
  updateDate: Date | string;
}
