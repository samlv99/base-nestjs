import { Body, Controller, Post } from '@nestjs/common';
import { MailScheduleService } from './mail-schedule.service';

@Controller('mail')
export class MailScheduleController {
  constructor(private mailScheduleService: MailScheduleService) {}

  @Post('/send-mail')
  async sendMail() {
    return await this.mailScheduleService.sendMail();
  }
}
