import { Module } from '@nestjs/common';
import { MailScheduleController } from './mail-schedule.controller';
import { MailScheduleService } from './mail-schedule.service';

@Module({
  controllers: [MailScheduleController],
  providers: [MailScheduleService],
})
export class MailModule {}
