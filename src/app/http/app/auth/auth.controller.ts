import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/register.dto';
import { RequesVertifycationCodeDto } from './dto/request-vertifycation-code.dto copy';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/request-vertifycation-code')
  async requestVerifycationCode(@Body() params: RequesVertifycationCodeDto) {
    return await this.authService.requestVerifycationCode(params);
  }

  @Post('/register')
  async register(@Body() params: RegisterDto) {
    return await this.authService.register(params);
  }
}
