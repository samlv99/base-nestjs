import { IsPhoneNumber, IsString } from 'class-validator';

export class RegisterDto {
  @IsPhoneNumber('VN')
  readonly mobile: string;

  @IsString()
  readonly code: string;
}
