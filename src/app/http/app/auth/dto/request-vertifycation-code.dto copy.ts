import { VerificationCodeType } from 'app/enums';
import { IsEnum, IsInt, IsPhoneNumber } from 'class-validator';

export class RequesVertifycationCodeDto {
  @IsPhoneNumber('VN')
  readonly mobile: string;

  @IsInt()
  @IsEnum(VerificationCodeType)
  readonly type: number;
}
