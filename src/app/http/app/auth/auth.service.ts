import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import User from 'app/entities/user.entity';
import VerificationCode from 'app/entities/verification-code.entity';
import { CommonStatus, Environment, ErrorCode, VerificationCodeType } from 'app/enums';
import { AppError, randomOTP } from 'app/helpers/utils';
import { GenerateRefreshTokenParams } from 'app/http/cms/auth/interface';
import to from 'await-to-js';
import { sign, verify } from 'jsonwebtoken';
import { pick } from 'lodash';
import { EntityManager, getConnection, getRepository, MoreThan } from 'typeorm';
import { promisify } from 'util';
import { RegisterDto } from './dto/register.dto';

const verifyAsync = promisify(verify) as any;

export interface VerifyOTPCodeParams {
  mobile: string;
  code: string;
  type: VerificationCodeType;
}

@Injectable()
export class AuthService {
  constructor(private readonly configService: ConfigService) {}

  async requestVerifycationCode({ mobile, type }) {
    const verificationCodeRepo = getRepository(VerificationCode);

    if (type === VerificationCodeType.Register) {
      const isExist = await this.isMobileExist(mobile);
      if (isExist) {
        return AppError(ErrorCode.MobileIsAlreadyExist);
      }
    }

    if (type === VerificationCodeType.Login) {
      const isExist = await this.isMobileExist(mobile);
      if (isExist) {
        return AppError(ErrorCode.MobileNotExist);
      }
    }

    const verificationCode = await verificationCodeRepo.findOne({
      where: {
        mobile,
        type,
        status: CommonStatus.Active,
        expireDate: MoreThan(new Date().toISOString()),
      },
    });

    if (verificationCode && verificationCode.retry >= 5) return AppError(ErrorCode.MaximumRetry);

    const code = randomOTP(6);
    const currentUnixTimeStamp = new Date().getTime();
    const expireDate = new Date(
      currentUnixTimeStamp + this.configService.get('auth.verificationCodeExpire')
    ).toISOString();

    if (verificationCode) {
      await verificationCodeRepo.update({ id: verificationCode.id }, { retry: verificationCode.retry + 1, code });
      if (this.configService.get('environment') !== Environment.Development) {
        // sendMessageSMS(type, phone, code);
        return { code };
      } else return { code };
    }

    await verificationCodeRepo.save({ mobile, code, type, expireDate });
    return { code };
  }

  async register({ mobile, code }: RegisterDto) {
    return await getConnection().transaction(async (transaction: EntityManager) => {
      const userRepo = transaction.getRepository(User);
      const verificationCodeRepo = transaction.getRepository(VerificationCode);

      const isExist = userRepo.findOne({ mobile });
      if (isExist) return AppError(ErrorCode.MobileIsAlreadyExist);

      const verifyOTP = await this.verifyOTPCode({ mobile, code, type: VerificationCodeType.Register }, transaction);
      if (!verifyOTP) return AppError(ErrorCode.VerificationCodeInvalid);

      await verificationCodeRepo.update({ id: verifyOTP.id }, { status: CommonStatus.InActive });

      const user = await userRepo.save({ mobile });
      const token = await this.generateToken(user.id, transaction);
    });
  }

  async isMobileExist(mobile: string, transaction?: EntityManager) {
    const userRepo = transaction ? transaction.getRepository(User) : getRepository(User);
    const user = await userRepo.count({ mobile });
    return !!user;
  }

  async verifyOTPCode({ mobile, code, type }: VerifyOTPCodeParams, transaction?: EntityManager) {
    const verificationCodeRepo = transaction
      ? transaction.getRepository(VerificationCode)
      : getRepository(VerificationCode);

    const isExist = await verificationCodeRepo.findOne({
      mobile,
      code,
      type,
      status: CommonStatus.Active,
      expireDate: MoreThan(new Date().toISOString()),
    });

    return isExist;
  }

  async generateToken(userId: number, transaction?: EntityManager) {
    const userRepo = transaction ? transaction.getRepository(User) : getRepository(User);
    const user = await this.getUserInformationById(userId, transaction);

    const dataEncode = pick(user, ['id', 'status']);
    const token = await this.generateAccessToken(dataEncode);
    const oldRefreshToken = user.refreshToken;

    const [error] = await to(verifyAsync(oldRefreshToken, this.configService.get('auth.refreshTokenSecret')));

    if (error) {
      const dataEncodeRefreshToken = pick(user, ['id', 'status']);
      const newRefreshToken = await this.generateRefreshToken(dataEncodeRefreshToken);
      userRepo.update(userId, { refreshToken: newRefreshToken });
      return { token, refreshToken: newRefreshToken };
    }

    return { token, refreshToken: oldRefreshToken };
  }

  async getUserInformationById(userId: number, transaction?: EntityManager) {
    const userRepo = transaction ? transaction.getRepository(User) : getRepository(User);
    return await userRepo.findOne({ id: userId });
  }

  async generateAccessToken(params: GenerateRefreshTokenParams) {
    return sign(params, this.configService.get('auth.accessTokenSecret'), {
      algorithm: 'HS256',
      expiresIn: this.configService.get('auth.accessTokenExpire'),
    });
  }

  async generateRefreshToken(params: GenerateRefreshTokenParams) {
    return sign(params, this.configService.get('auth.refreshTokenSecret'), {
      algorithm: 'HS256',
      expiresIn: this.configService.get('auth.refreshTokenExpire'),
    });
  }
}
