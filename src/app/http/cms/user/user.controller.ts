import { Controller, Get, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { AppAccessTokenGuard } from '../auth/guards/access-token.guard';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @UseGuards(AppAccessTokenGuard)
  @Get('/me')
  async getMyInformation(@Req() req: Request) {
    return await this.userService.getMyInformation(Number(req.user));
  }
}
