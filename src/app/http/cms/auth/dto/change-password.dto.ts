import { IsOptional, IsString } from 'class-validator';

export class ChangePasswordDto {
  @IsString()
  @IsOptional()
  readonly oldPassword: string;

  @IsString()
  @IsOptional()
  readonly newPassword: string;
}
