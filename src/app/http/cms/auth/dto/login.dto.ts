import { ErrorMessage } from 'app/enums';
import { IsEmail, IsNotEmpty, IsOptional, IsString, MinLength } from 'class-validator';

export class LoginDto {
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(6, { message: ErrorMessage.PasswordIsTooShort })
  readonly password: string;

  @IsString()
  @IsOptional()
  readonly token: string;
}
