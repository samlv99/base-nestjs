import { IsOptional, IsString } from 'class-validator';

export class RequestAccessTokenDto {
  @IsString()
  @IsOptional()
  readonly refreshToken: string;
}
