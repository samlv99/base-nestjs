import { HttpStatus, Injectable } from '@nestjs/common';
import { hashSync, genSaltSync, compareSync } from 'bcryptjs';
import { sign, verify } from 'jsonwebtoken';
import { getRepository } from 'typeorm';
import { promisify } from 'util';
import to from 'await-to-js';
import { pick } from 'lodash';
import { CommonStatus, ErrorCode } from 'app/enums';
import User from 'app/entities/user.entity';
import { AppError } from 'app/helpers/utils';
import { CreateUserDto } from './dto/create-user.dto';
import { ConfigService } from '@nestjs/config';
import { GenerateRefreshTokenParams } from './interface';
import { LoginDto } from './dto/login.dto';
import { RequestAccessTokenDto } from './dto/request-access-token.dto';
import { ChangePasswordDto } from './dto/change-password.dto';

const verifyAsync = promisify(verify) as any;

@Injectable()
export class AuthService {
  constructor(private readonly configService: ConfigService) {}
  async register({ email, password }: CreateUserDto) {
    const userRepo = getRepository(User);
    const isExist = await userRepo.findOne({ email });
    if (isExist) return AppError(ErrorCode.EmailIsAlreadyExist);

    const salt = genSaltSync(this.configService.get('auth.saltRounds'));
    const hashPassword = hashSync(password, salt);
    const user = await userRepo.save({ email, password: hashPassword });
    const token = await this.generateToken(user.id);
    return { userId: user.id, token };
  }

  async login({ email, password }: LoginDto) {
    const userRepo = getRepository(User);
    const user = await userRepo.findOne({ email });
    if (!user) return AppError(ErrorCode.UserNotExits);

    const isTruePassword = compareSync(password, user.password);
    if (!isTruePassword) return AppError(ErrorCode.PasswordIsIncorector);

    return {
      UserId: user.id,
      token: await this.generateToken(user.id),
    };
  }

  async requestAccessToken({ refreshToken }: RequestAccessTokenDto) {
    const userRepo = getRepository(User);
    if (!refreshToken) return AppError(ErrorCode.RefreshTokenNotExits);

    try {
      const decode = verify(refreshToken, this.configService.get('auth.refreshTokenSecret')) as any;
      const user = await userRepo.findOne({ id: decode.id });
      if (!user) {
        return AppError(ErrorCode.UserNotExits);
      }
      if (user.status === CommonStatus.InActive) {
        return AppError(ErrorCode.UserBlocked);
      }

      const dataEncode = pick(user, ['id', 'status']);
      const token = await this.generateAccessToken(dataEncode);

      return { token };
    } catch (error) {
      return AppError(ErrorCode.InvalidSignature, HttpStatus.UNAUTHORIZED);
    }
  }

  async changePassword(userId: number, { oldPassword, newPassword }: ChangePasswordDto) {
    const userRepo = getRepository(User);
    const user = await userRepo.findOne({ id: userId });

    if (!user) {
      return AppError(ErrorCode.UserNotExits, HttpStatus.BAD_REQUEST);
    }

    if (user.status === CommonStatus.InActive) {
      return AppError(ErrorCode.UserBlocked, HttpStatus.BAD_REQUEST);
    }

    if (oldPassword === newPassword) {
      return AppError(ErrorCode.NewPasswordIsNotSameOldPassword, HttpStatus.BAD_REQUEST);
    }

    const isTruePassword = compareSync(oldPassword, user.password);
    if (!isTruePassword) {
      return AppError(ErrorCode.PasswordIsIncorector, HttpStatus.BAD_REQUEST);
    }

    const salt = genSaltSync(this.configService.get('auth.saltRounds'));
    const hashPassword = hashSync(newPassword, salt);
    await userRepo.update({ id: userId }, { password: hashPassword });
    return;
  }

  async generateToken(userId: number) {
    const userRepo = getRepository(User);
    const user = await userRepo.findOne({ id: userId });

    const dataEncode = pick(user, ['id', 'status']);

    const token = await this.generateAccessToken(dataEncode);
    const oldRefreshToken = user.refreshToken;

    const [error] = await to(verifyAsync(oldRefreshToken, this.configService.get('auth.refreshTokenSecret')));
    if (error) {
      const dataEncodeRefreshToken = pick(user, ['id', 'status']);
      const newRefreshToken = await this.generateRefreshToken(dataEncodeRefreshToken);
      userRepo.update(userId, { refreshToken: newRefreshToken });
      return { token, refreshToken: newRefreshToken };
    }
    return { token, refreshToken: oldRefreshToken };
  }

  async generateAccessToken(params: GenerateRefreshTokenParams) {
    return sign(params, this.configService.get('auth.accessTokenSecret'), {
      algorithm: 'HS256',
      expiresIn: this.configService.get('auth.accessTokenExpire'),
    });
  }

  async generateRefreshToken(params: GenerateRefreshTokenParams) {
    return sign(params, this.configService.get('auth.refreshTokenSecret'), {
      algorithm: 'HS256',
      expiresIn: this.configService.get('auth.refreshTokenExpire'),
    });
  }

  async loginFacebook() {}
}
