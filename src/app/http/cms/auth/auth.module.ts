import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import config from 'config';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AppRefreshTokenStrategy } from './strategies';
import { AppAccessTokenStrategy } from './strategies/access-token.strategy';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [config],
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, AppAccessTokenStrategy, AppRefreshTokenStrategy],
})
export class CmsAuthModule {}
