import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ErrorCode } from 'app/enums';
import { AppError } from 'app/helpers/utils';
import { JsonWebTokenError, TokenExpiredError } from 'jsonwebtoken';

@Injectable()
export class AppAccessTokenGuard extends AuthGuard('app-access-token') {
  constructor() {
    super();
  }
  handleRequest(err: any, user: any, info: any) {
    if (info instanceof TokenExpiredError) {
      return AppError(ErrorCode.TokenExpired, HttpStatus.UNAUTHORIZED);
    }
    if (info instanceof JsonWebTokenError) {
      return AppError(ErrorCode.AccessTokenInvalid, HttpStatus.UNAUTHORIZED);
    }
    if (err instanceof HttpException) {
      const { response, status } = err as any;
      return AppError(response.errorCode, status);
    }

    return user;
  }
}
