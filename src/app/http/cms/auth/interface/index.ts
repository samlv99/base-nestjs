import { CommonStatus } from 'app/enums';

export interface GenerateRefreshTokenParams {
  id: number;
  status: CommonStatus;
}
