import { HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import User from 'app/entities/user.entity';
import { CommonStatus, ErrorCode } from 'app/enums';
import { AppError } from 'app/helpers/utils';
import { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { getRepository } from 'typeorm';

export interface Payload {
  id: number;
  status: CommonStatus;
  [key: string]: any;
}

@Injectable()
export class AppAccessTokenStrategy extends PassportStrategy(Strategy, 'app-access-token') {
  constructor(private readonly configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request: Request) => {
          let token = request.headers['authorization'] || '';
          if (!token) {
            AppError(ErrorCode.AccessTokenNotExist, HttpStatus.UNAUTHORIZED);
          }
          token = token.replace('Bearer ', '');

          return token;
        },
      ]),
      secretOrKey: configService.get('auth.accessTokenSecret'),
      ignoreExpiration: false,
    });
  }

  async validate(payload: Payload) {
    const userRepo = getRepository(User);
    const user = await userRepo.findOne({ where: { id: payload.id } });
    if (user.status === CommonStatus.InActive) {
      return AppError(ErrorCode.UserBlocked, HttpStatus.UNAUTHORIZED);
    }

    return user.id;
  }
}
