import { HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import User from 'app/entities/user.entity';
import { CommonStatus, ErrorCode } from 'app/enums';
import { AppError } from 'app/helpers/utils';
import { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { getRepository } from 'typeorm';
import { Payload } from './access-token.strategy';

@Injectable()
export class AppRefreshTokenStrategy extends PassportStrategy(Strategy, 'app-refresh-token') {
  constructor(private readonly configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request: Request) => {
          const token = request.body.refreshToken || '';
          if (!token) {
            AppError(ErrorCode.AccessTokenNotExist, HttpStatus.UNAUTHORIZED);
          }

          return token;
        },
      ]),
      secretOrKey: configService.get('auth.refreshTokenSecret'),
      ignoreExpiration: false,
    });
  }

  async validate(payload: Payload) {
    const userRepo = getRepository(User);
    const user = await userRepo.findOne({ id: payload.id });

    if (!user) {
      return AppError(ErrorCode.UserNotExits, HttpStatus.UNAUTHORIZED);
    }
    if (user.status === CommonStatus.InActive) {
      return AppError(ErrorCode.UserBlocked, HttpStatus.UNAUTHORIZED);
    }

    return user.id;
  }
}
