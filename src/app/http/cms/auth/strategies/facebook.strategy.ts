import { HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import User from 'app/entities/user.entity';
import { CommonStatus, ErrorCode } from 'app/enums';
import { AppError } from 'app/helpers/utils';
import { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { getRepository } from 'typeorm';

export interface Payload {
  id: number;
  status: CommonStatus;
  [key: string]: any;
}

@Injectable()
export class FacebookStrategy extends PassportStrategy(Strategy, 'facebook') {
  async validate(payload: Payload) {
    const userRepo = getRepository(User);
    const user = await userRepo.findOne({ where: { id: payload.id } });
    if (user.status === CommonStatus.InActive) {
      return AppError(ErrorCode.UserBlocked, HttpStatus.UNAUTHORIZED);
    }

    return user.id;
  }
}
