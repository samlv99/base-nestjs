import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ChangePasswordDto } from './dto/change-password.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginDto } from './dto/login.dto';
import { RequestAccessTokenDto } from './dto/request-access-token.dto';
import { AppAccessTokenGuard } from './guards';
import { FacebookGuard } from './guards/facebook.guard';
import AppRefreshTokenGuard from './guards/refresh-token.guard';

@Controller('cms/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/register')
  async register(@Body() params: CreateUserDto) {
    return this.authService.register(params);
  }

  @Post('/login')
  async login(@Body() params: LoginDto) {
    return this.authService.login(params);
  }

  @UseGuards(AppRefreshTokenGuard)
  @Post('/request-access-token')
  async requestAccessToken(@Body() params: RequestAccessTokenDto) {
    return this.authService.requestAccessToken(params);
  }

  @UseGuards(AppAccessTokenGuard)
  @Post('/change-password')
  async changePassword(@Req() req, params: ChangePasswordDto) {
    return this.authService.changePassword(Number(req.userId), params);
  }

  @UseGuards(FacebookGuard)
  @Get('/login-facebook')
  async loginFacebook() {
    return await this.authService.loginFacebook();
  }
}
